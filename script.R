library(car)
library(gplots)
library(xlsx)
library(ggplot2)
# загрузка данных по веществам и болезням
# дети - 22:30, общее - 8:16, подростки -36:44 , взрослые - 50:58
kids = 22:30
all = 8:16
teen = 36:44
growth = 50:58
morbTrans = getMorbTrans(kids)

# парные графики для болезней
pairs(morbTrans[, 3:22])

# берем обший уровень загрязнений за все время
totalFile = read.csv(file = "Data/Общие показатели.csv", header = T, sep = ";")
total = as.data.frame(t(totalFile[1:6, 2:(ncol(totalFile))]))
colnames(total) = c("sanitary", "microbe", "noise", "illumination", "vibration", "EMF")

# генерируем общие показатели (ИСА,СИ, НП, УРОВЕНь)

achinsk = readCityIsa("Data/Ачинск.csv")
kansk = readCityIsa("Data/Канск.csv")
kras = readCityIsa("Data/Красноярск.csv")
les = readCityIsa("Data/Лесосибирск.csv")
minus = readCityIsa("Data/Минусинск.csv")
nazar = readCityIsa("Data/Назарово.csv")

detailStaff = read.prec.data()

m1 = get.cor.dataframes(achinsk, achinskMorbAll)
m2 = get.cor.dataframes(kansk, kanskMorbAll)
m3 = get.cor.dataframes(kras, KrasMorbAll)
m4 = get.cor.dataframes(les, lesMorbAll)
m5 = get.cor.dataframes(minus, minusMorbAll)
m6 = get.cor.dataframes(nazar, nazarMorbAll)
cors = list(m1, m2, m3, m4, m5, m6)
mp1 = get.cor.p.dataframes(achinsk, achinskMorbAll)
mp2 = get.cor.p.dataframes(kansk, kanskMorbAll)
mp3 = get.cor.p.dataframes(kras, KrasMorbAll)
mp4 = get.cor.p.dataframes(les, lesMorbAll)
mp5 = get.cor.p.dataframes(minus, minusMorbAll)
mp6 = get.cor.p.dataframes(nazar, nazarMorbAll)
corsP = list(mp1, mp2, mp3, mp4, mp5, mp6)

m = array(NA, c(length(cors), nrow(cors[[1]]), ncol(cors[[1]])))
p = array(NA, c(length(cors), nrow(cors[[1]]), ncol(cors[[1]])))

for (i in 1:length(cors)){
  for (j in 1:nrow(cors[[i]]))
    for (k in 1:ncol(cors[[i]])){
      m[i, j, k] = cors[[i]][j,k]
      p[i, j, k] = corsP[[i]][j,k]
    }
}

means = matrix(NA, nrow(m1), ncol(m1))
sds = matrix(NA, nrow(m1), ncol(m1))
meansP = matrix(NA, nrow(m1), ncol(m1))

for (i in 1:nrow(m1))
  for (j in 1:ncol(m1)) {
    means[i, j] = mean(m[, i, j])
    sds[i, j] = sd(m[, i, j])
    meansP[i, j] = mean(p[, i, j])
  }
  
png(filename="Data/img/Средняя корреляция между болезнями и ИСА5.png", width=30, height=10, units='in', res=300)
heatmap.2(meansP,           # cell labeling
          cellnote=round(means,2),
          notecex=2.0,
          notecol="black",
          na.color=par("bg"), dendrogram="none", Colv = FALSE, Rowv = FALSE)

dev.off()

# рисование графиков зависимостей болезней и веществ по городам во всех годах
y = "Новообразования"
x = "isaВзвешенные"
y = "Травмы.и.отравления"
getAngleForAllYears(x, y, needToCor = T)

y = "Болезни.кожи.и.подкожной.клетчатки"
x = "avФенол"
plotLinearModelsForGroup(x, y)

plotStaffAndMorb = function(x, y){
  col = c("#c70000", "#ffe500", "#2ab300", "#009eb3", "#0c00b3", "#b300a1")
  t = morbTrans
  d = split(t, t$place)
  delta = 3
  plot(c(), xlim = c(1, 12), ylim = c(-2, length(d) * delta), t = "n")
  for (i in 1:length(d)){
      lines(scale(d[[i]][x]) + (i - 1) * delta, col = col[i], lty = "dashed")
      lines(scale(d[[i]][y]) + (i - 1) * delta, col = col[i])
      text(4, ((scale(d[[i]][y]) + (i - 1) * delta - scale(d[[i]][x]) + (i - 1) * delta) / 2)[4], tryCatch( 
        {  round(cor(d[[i]][x], d[[i]][y], use="complete.obs"), 2) }, error = function(e) { 0 }, pos = 1))
  }
  legend(9,15, c("Ачинск", "Канск", "Красноярск", "Лесосибирск", "Минусинск", "Назарово", "Пунктир-вешества"), lty=c(1, 1, 1, 1, 1, 1, 1), 
         lwd=c(2.5,2.5), col= c(col, 1), cex = 1, box.lty=0)
}

png(filename="Data/img/plots/t.png", width=1000, height=1000, res=100)
plotStaffAndMorb(x, y)
dev.off()
write.xlsx(morbTrans, "Data/Заболеваемость детей и количество веществ.xlsx")
write.xlsx(morbTrans, "Data/Заболеваемость всех и количество веществ.xlsx")
write.xlsx(morbTrans, "Data/Заболеваемость подростков и количество веществ.xlsx")
write.xlsx(morbTrans, "Data/Заболеваемость взрослых и количество веществ.xlsx")


morbTrans = getMorbTrans(kids)
morbTrans = getMorbTrans(all)
morbTrans = getMorbTrans(teen)
morbTrans = getMorbTrans(growth)
morbsLabels = colnames(morbTrans)[3:22]
staffs = colnames(morbTrans)[23:70]
for (morbT in morbsLabels){
  for (staffsT in staffs) {
    png(filename=paste("Data/img/plots/teen/", morbT, "-", staffsT, ".png", sep = ""), width=1000, height=1000, res=100)
    plotStaffAndMorb(staffsT, morbT)
    dev.off()
  }
}

# рисует линии регрессий для каждого города по всем годам
plotLinearModelsForGroup(x, y)

plotLinearModelForOneYear = function(x, y, year){
  d = morbTrans[morbTrans$year == year, ]
  plot(d[[x]], d[[y]])
}
plotLinearModelForOneYear(x, y, 2007)

m = lm(as.formula(morbTrans[1:47, 4] ~ morbTrans[1:47, 34] + morbTrans[1:47, 2] + 1))
m = lm(as.formula(morbTrans[1:47, 4] ~ morbTrans[1:47, 2] / (1 + morbTrans[1:47, 34]) - 1))

# создание таблицы корреляций для всех городов и годов
for(i in 1:ncol(morbTrans))
  morbTrans[is.na(morbTrans[,i]), i] <- mean(morbTrans[,i], na.rm = TRUE)

df1 = morbTrans[, 3:22]
df2 = morbTrans[, 23:70]
t = list()
t$means = get.cor.dataframes(df1, df2)
t$sds = get.cor.p.dataframes(df1, df2)
colnames(t$means) = colnames(morbTrans)[23:70]
rownames(t$means) = colnames(morbTrans)[3:22]
colnames(t$sds) = colnames(morbTrans)[23:70]
rownames(t$sds) = colnames(morbTrans)[3:22]

kids = 22:30
all = 8:16
teen = 36:44
growth = 50:58
morbTrans = getMorbTrans(kids)
morbTrans = getMorbTrans(all)
morbTrans = getMorbTrans(teen)
morbTrans = getMorbTrans(growth)

t = getAngleMeanForAllYears(T)
t = createModelsCoefs(morbTrans)

png(filename="Data/img/Значения средней корреляций и дисперсий по годам для детей.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по годам для детей.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по годам для всех.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по годам для всех.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по годам для подростков.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по годам для подростков.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по годам для взрослых.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по годам для взрослых.png", width=30, height=20, units='in', res=300)

png(filename="Data/img/Значения средней корреляций и дисперсий по городам для детей.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по городам для детей.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по городам для всех.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по городам для всех.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по городам для подростков.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по городам для подростков.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и дисперсий по городам для взрослых.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений по городам для взрослых.png", width=30, height=20, units='in', res=300)

png(filename="Data/img/Значения средней корреляций и p-значений для взрослых.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений для детей.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений для всех.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения средней корреляций и p-значений для подростков.png", width=30, height=20, units='in', res=300)


heatmap.2(t(t$sds),           # cell labeling
          cellnote=t(round(t$means,2)),
          notecex=1.0,
          notecol="black",
          na.color=par("bg"), dendrogram="none", Colv = FALSE, Rowv = FALSE, margins=c(20,8))

dev.off()

write.xlsx(t$means, "Data/Значения средней корреляций по годам для детей.xlsx")
write.xlsx(t$sds, "Data/Значения дисперсии корреляций по годам для детей.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций по годам для детей.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций по годам для всех.xlsx")
write.xlsx(t$sds, "Data/Значения дисперсии корреляций по годам для всех.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций по годам для всех.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций по годам для подростков.xlsx")
write.xlsx(t$sds, "Data/Значения дисперсии корреляций по годам для подростков.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций по годам для подростков.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций по годам для взрослых.xlsx")
write.xlsx(t$sds, "Data/Значения дисперсии корреляций по годам для взрослых.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций по годам для взрослых.xlsx")

heatmap.2(t(t$corSds),           # cell labeling
          cellnote=t(round(t$corMean, 2)),
          notecex=1.0,
          notecol="black",
          na.color=par("bg"), dendrogram="none", Colv = FALSE, Rowv = FALSE, margins=c(20,8))

dev.off()


write.xlsx(t$corMean, "Data/Значения средней корреляций по городам для детей.xlsx")
write.xlsx(t$corSds, "Data/Значения дисперсии корреляций по городам для детей.xlsx")
write.xlsx(t$corSds, "Data/Значения p-значений корреляций по городам для детей.xlsx")
write.xlsx(t$corMean, "Data/Значения средней корреляций по городам для всех.xlsx")
write.xlsx(t$corSds, "Data/Значения дисперсии корреляций по городам для всех.xlsx")
write.xlsx(t$corSds, "Data/Значения p-значений корреляций по городам для всех.xlsx")
write.xlsx(t$corMean, "Data/Значения средней корреляций по городам для подростков.xlsx")
write.xlsx(t$corSds, "Data/Значения дисперсии корреляций по городам для подростков.xlsx")
write.xlsx(t$corSds, "Data/Значения p-значений корреляций по городам для подростков.xlsx")
write.xlsx(t$corMean, "Data/Значения средней корреляций по городам для взрослых.xlsx")
write.xlsx(t$corSds, "Data/Значения дисперсии корреляций по городам для взрослых.xlsx")
write.xlsx(t$corSds, "Data/Значения p-значений корреляций по городам для взрослых.xlsx")






write.xlsx(t$means, "Data/Значения средней корреляций для взрослых.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций для взрослых.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций для детей.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций для детей.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций для всех.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций для всех.xlsx")
write.xlsx(t$means, "Data/Значения средней корреляций для подростков.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций для подростков.xlsx")

# корреляция между заболеваемостью и веществами по городам. При этом считаюстя все возраста
t = printCorrelationForAllAges()
png(filename="Data/img/Значения корреляций и дисперсий по городам для всех возрастов.png", width=30, height=20, units='in', res=300)
png(filename="Data/img/Значения корреляций и p-значений по городам для всех возрастов.png", width=30, height=20, units='in', res=300)
heatmap.2(t(t$sds),           # cell labeling
          cellnote=t(round(t$means,2)),
          notecex=1.0,
          notecol="black",
          na.color=par("bg"), dendrogram="none", Colv = FALSE, Rowv = FALSE, margins=c(20,8))
dev.off()
write.xlsx(t$means, "Data/Значения средней корреляций по годам для взрослых.xlsx")
write.xlsx(t$sds, "Data/Значения дисперсии корреляций по годам для взрослых.xlsx")
write.xlsx(t$sds, "Data/Значения p-значений корреляций по годам для взрослых.xlsx")

# получаем синхронность
getSynchronicity = function(firstVector, secondVector){
  if (length(firstVector) != length(secondVector))
    stop("Error in getting synchronicity. Length of two vector are different")
  x = rep(0, length(firstVector) - 1)
  for (i in 1:(length(x) - 1)){
    if (firstVector[i] > firstVector[i+1] && secondVector[i] > secondVector[i+1] |
        firstVector[i] < firstVector[i+1] && secondVector[i] < secondVector[i+1] |
        firstVector[i] == firstVector[i+1] && secondVector[i] == secondVector[i+1] ) {
      x[i] = 1
    } else {
      x[i] = 0
    }
  }
  return (mean(x))
}

get.syn.dataframes = function(frame1, frame2){
  m = matrix(NA, ncol(frame1), ncol(frame2))
  for (i in 1:ncol(frame1))
    for (j in 1:ncol(frame2))
      if (!is.numeric(frame1[, i]) || !is.numeric(frame2[, j]) || sd(frame1[, i]) == 0 || sd(frame2[, j]) == 0 )
        m[i,j] = 0
      else
        m[i,j] = getSynchronicity(frame1[, i], frame2[, j])
      
      return(m)
}

m1 = get.syn.dataframes(achinsk, achinskMorbAll)
m2 = get.syn.dataframes(kansk, kanskMorbAll)
m3 = get.syn.dataframes(kras, KrasMorbAll)
m4 = get.syn.dataframes(les, lesMorbAll)
m5 = get.syn.dataframes(minus, minusMorbAll)
m6 = get.syn.dataframes(nazar, nazarMorbAll)
syns = list(m1, m2, m3, m4, m5, m6)

m = array(NA, c(length(syns), nrow(syns[[1]]), ncol(syns[[1]])))
for (i in 1:length(syns)){
  for (j in 1:nrow(syns[[i]]))
    for (k in 1:ncol(syns[[i]]))
      m[i, j, k] = syns[[i]][j,k]
}

means = matrix(NA, nrow(m1), ncol(m1))
sds = matrix(NA, nrow(m1), ncol(m1))

for (i in 1:nrow(m1))
  for (j in 1:ncol(m1)) {
    means[i, j] = mean(m[, i, j])
    sds[i, j] = sd(m[, i, j])
  }
png(filename="Data/img/Средняя корреляция между болезнями и ИСА1.png", width=30, height=10, units='in', res=300)
heatmap.2(sds,           # cell labeling
          cellnote=round(means,2),
          notecex=2.0,
          notecol="black",
          na.color=par("bg"), dendrogram="none", Colv = FALSE, Rowv = FALSE)

dev.off()




# болезни для всех регионов для кластеризации
morbAllRegion = read.csv("Data/Заболеваемость для всех регионов.csv", sep = ";")
colnames(morbAllRegion)[1] = "place"
colnames(morbAllRegion)[2] = "morb"
morbAllRegion$place = trimws(morbAllRegion$place)
morbAllRegion$morb = trimws(morbAllRegion$morb)

morbs = 2:4
years = 3:4
getDataToCluster = function(morbs, years, scale = T){
  rows = vector()
  for (i in morbs){
    t = seq(i, nrow(morbAllRegion), 20)
    rows = c(rows, t)
  }
  rows = sort(rows)
  t = morbAllRegion[rows, c(1:2, years)]
  res = data.frame(t[seq(1,nrow(t), length(morbs)), 1:2])
  for (i in 1:length(morbs)) {
    t1 = t[seq(i,nrow(t), length(morbs)),c(-1, -2)]
    res = cbind(res, t1)
  }
  res = res[, -2]
  
  yearLabels = (years - 3)%%14 + 2002
  morbsLabels = morbAllRegion$morb[morbs]
  labs = c()
  
  for (j in morbsLabels)
    for (i in yearLabels)
      labs = c(labs, paste(j, i, sep = "."))
  
  colnames(res)[-1] = labs
  if (!scale)
    return(res)
  
  for (k in 1:nrow(res)){
    for (i in 1:length(years)){
      sum = sum(res[k, seq(1 + i, ncol(res), length(years))])
      for (j in seq(1 + i, ncol(res), length(years)))
        res[k, j] = res[k, j] / sum
    }
  }
  
  res[, -1] = sapply(res[, -1], as.numeric )
  
  return (res)
  
}
res = getDataToCluster(c(3, 11, 12), 16)

resA = getDataToCluster(c(3, 11, 12), 16)
resK = getDataToCluster(c(3, 11, 12), 30)
resT = getDataToCluster(c(3, 11, 12), 44)
resG = getDataToCluster(c(3, 11, 12), 58)
resA[, 1] = paste(resA[, 1], "All", sep = ".")
resK[, 1] = paste(resK[, 1], "Kids", sep = ".")
resT[, 1] = paste(resT[, 1], "Teen", sep = ".")
resG[, 1] = paste(resG[, 1], "Adult", sep = ".")
res = rbind(resA, resK, resT, resG)


forSom = res[rep(seq_len(nrow(res)), each=30),]
forSom = forSom[sample(nrow(forSom)), ]
forSom[, 1] = rep("", nrow(forSom))
forSom[1:nrow(res), 1] = res[, 1]
som_grid <- somgrid(xdim = 30, ydim=30, topo="hexagonal")
sommod = som(as.matrix(forSom[, -1]), grid = som_grid)
plot(sommod, type="codes")
plot(sommod, type="mapping", labels = forSom[, 1])

t = res[, -1]
rownames(t) = seq(1, nrow(res))
stab <- clValid(t, 2:10, clMethods=c("kmeans"),
                validation="stability")
optimalScores(stab)
plot(stab)


